---
version: '2.0'

name: devel_identity
description: Workbook that contains identity workflows

workflows:
  fetch_regions:
    description: Fetch regions available for workflows
    type: direct
    tasks:
      fetch_regions:
        action: keystone.regions_list
        input:
          action_region: 'next'
        publish:
          regions: <% task(fetch_regions).result.select(dict(id => $.id, env => $.description.split(","))).where('devel_lifecycle' in $.env).id %>
    output:
      regions: <% $.regions %>

  get_default_region:
    description: Get default region
    type: direct
    tasks:
      fetch_regions:
        action: keystone.regions_list
        input:
          action_region: 'next'
        publish:
          region: <% task(fetch_regions).result.select(dict(id => $.id, env => $.description.split(","))).where('devel_default' in $.env).id.first() %>
    output:
      region: <% $.region %>

  project_create:
    description: Creates a Project, assigns the ownership/membership
    type: direct
    input:
      - id
      - name
      - description
      - status
      - type
      - enabled
      - owner
      - administrator
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - project_create_per_region
      project_create_per_region:
        with-items: region in <% $.regions %>
        workflow: project_create_per_region
        input:
          id: <% $.id %>
          name: <% $.name %>
          description: <% $.description %>
          status: <% $.status %>
          type: <% $.type %>
          enabled: <% $.enabled %>
          owner: <% $.owner %>
          administrator: <% $.administrator %>
          region: <% $.region %>
    output:
      id: <% $.id %>
      name: <% $.name %>
      description: <% $.description %>
      status: <% $.status %>
      type: <% $.type %>
      enabled: <% $.enabled %>
      owner: <% $.owner %>

  project_create_per_region:
    description: Creates a Project, assigns the ownership/membership
    type: direct
    input:
      - id
      - name
      - description
      - status
      - type
      - enabled
      - owner
      - administrator
      - region
    output:
      id: <% $.project_id %>
      name: <% $.name %>
      description: <% $.description %>
      status: <% $.status %>
      type: <% $.type %>
      enabled: <% $.enabled %>
      owner: <% $.owner %>
      administrator: <% $.administrator %>
    tasks:
      project_exists:
        action: keystone.projects_get
        input:
          action_region: <% $.region %>
          project: <% $.id %>
        publish:
          project_id: <% task(project_exists).result.id %>
          administrator: ''
        on-success:
          - configure_endpoint_groups
          - get_owner_role_id
          - set_shared_properties: <% not ('Personal' in $.name) %>
          - set_personal_properties: <% 'Personal' in $.name %>
        on-error:
          - create_project

      create_project:
        action: keystone.projects_create
        input:
          action_region: <% $.region %>
          id: <% $.id %>
          name: <% $.name %>
          domain: default
          description: <% $.description %>
          status: <% $.status %>
          type: <% $.type %>
          enabled: <% $.enabled %>
        publish:
          project_id: <% task(create_project).result.id %>
          administrator: ''
        on-success:
          - set_fim_sync_property

      set_fim_sync_property:
        action: keystone.projects_update
        input:
          action_region: <% $.region %>
          project: <% $.project_id %>
          fim-sync: 'False'
        on-success:
          - configure_endpoint_groups
          - get_owner_role_id
          - get_administrator_role_id: <% not $.administrator.isEmpty() %>
          - set_shared_properties: <% not ('Personal' in $.name) %>
          - set_personal_properties: <% 'Personal' in $.name %>

      configure_endpoint_groups:
        workflow: configure_endpoint_groups
        input:
          project: <% $.project_id %>
          personal: <% 'Personal' in $.name %>
          action_region: <% $.region %>

      get_owner_role_id:
        action: keystone.roles_find
        input:
          action_region: <% $.region %>
          name: 'owner'
        publish:
          owner_role_id: <% task(get_owner_role_id).result.id %>
        on-success:
          - set_project_owner

      set_project_owner:
        action: keystone.roles_grant
        input:
          action_region: <% $.region %>
          role: <% $.owner_role_id %>
          user: <% $.owner %>
          project: <% $.project_id %>

      get_administrator_role_id:
        action: keystone.roles_find
        input:
          action_region: <% $.region %>
          name: 'coordinator'
        publish:
          administrator_role_id: <% task(get_administrator_role_id).result.id %>
        on-success:
          - set_project_administrator

      set_project_administrator:
        action: keystone.roles_grant
        input:
          action_region: <% $.region %>
          role: <% $.administrator_role_id %>
          group: <% $.administrator %>
          project: <% $.project_id %>

      set_shared_properties:
        action: keystone.projects_update
        input:
          action_region: <% $.region %>
          project: <% $.project_id %>
          type: <% $.type %>
          fim-lock: 'True'
          fim-skip: 'True'

      set_personal_properties:
        action: keystone.projects_update
        input:
          action_region: <% $.region %>
          project: <% $.project_id %>
          chargerole: 'default'
          type: 'personal'
          network: 'GPN'
          tags:
            - 'expiration'

  project_get:
    description: Retrieves the project in the default region
    type: direct
    input:
      - id
    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - project_get
      project_get:
        action: keystone.projects_get
        input:
          action_region: <% $.default_region %>
          project: <% $.id %>
    output:
      project: <% task(project_get).result %>

  project_complete_creation:
    description: Retrieves the project in all regions
    type: direct
    input:
      - project
      - enabled
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - finalize_creation
      finalize_creation:
        action: keystone.projects_update
        with-items: region in <% $.regions %>
        input:
          action_region: <% $.region %>
          project: <% $.project %>
          enabled: <% $.enabled %>
          fim-sync: 'True'

  project_delete:
    description: Delete the project in all regions
    type: direct
    input:
      - id
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - delete_project
      delete_project:
        action: keystone.projects_delete
        with-items: region in <% $.regions %>
        input:
          project: <% $.id %>
          action_region: <% $.region %>

  configure_endpoint_groups:
    description: Configures endpoint groups in regions
    type: direct
    input:
      - project
      - personal
      - action_region
      - personal_egs:
        - 'base'
      - shared_egs: []
    tasks:
      add_personal_endpoint_groups:
        with-items: endpoint_group in <% $.personal_egs %>
        workflow: add_endpoint_group_per_region
        input:
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group %>
          action_region: <% $.action_region %>
        on-success:
          - add_shared_endpoint_groups: <% not $.personal %>

      add_shared_endpoint_groups:
        with-items: endpoint_group in <% $.shared_egs %>
        workflow: add_endpoint_group_per_region
        input:
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group %>
          action_region: <% $.action_region %>

  add_endpoint_group:
    description: Configures endpoint groups in regions
    type: direct
    input:
      - project
      - endpoint_group
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - add_endpoint_group_in_all_regions
      add_endpoint_group_in_all_regions:
        with-items: region in <% $.regions %>
        workflow: add_endpoint_group_per_region
        input:
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group %>
          action_region: <% $.region %>

  add_endpoint_group_per_region:
    description: Configures endpoint groups in regions
    type: direct
    input:
      - project
      - endpoint_group
      - action_region
    tasks:
      get_endpoint_group:
        action: keystone.endpoint_groups_find
        input:
          action_region: <% $.action_region %>
          name: <% $.endpoint_group %>
        publish:
          endpoint_group_id: <% task(get_endpoint_group).result.id %>
        on-success:
          - check_endpoint_group

      check_endpoint_group:
        action: keystone.endpoint_filter_check_endpoint_group_in_project
        input:
          action_region: <% $.action_region %>
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group_id %>
        on-error:
          - add_endpoint_group

      add_endpoint_group:
        action: keystone.endpoint_filter_add_endpoint_group_to_project
        input:
          action_region: <% $.action_region %>
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group_id %>

  remove_endpoint_group:
    description: Configures endpoint groups in regions
    type: direct
    input:
      - project
      - endpoint_group
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - remove_endpoint_group_in_all_regions
      remove_endpoint_group_in_all_regions:
        with-items: region in <% $.regions %>
        workflow: remove_endpoint_group_per_region
        input:
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group %>
          action_region: <% $.region %>

  remove_endpoint_group_per_region:
    description: Removes endpoint groups in regions
    type: direct
    input:
      - project
      - endpoint_group
      - action_region
    tasks:
      get_endpoint_group:
        action: keystone.endpoint_groups_find
        input:
          action_region: <% $.action_region %>
          name: <% $.endpoint_group %>
        publish:
          endpoint_group_id: <% task(get_endpoint_group).result.id %>
        on-success:
          - remove_endpoint_group

      remove_endpoint_group:
        action: keystone.endpoint_filter_delete_endpoint_group_from_project
        input:
          action_region: <% $.action_region %>
          project: <% $.project %>
          endpoint_group: <% $.endpoint_group_id %>

  project_update:
    description: Disables a project in all regions
    type: direct
    input:
      - id
      - name
      - description
      - status
      - type
      - owner
      - administrator
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - project_update
      project_update:
        with-items: region in <% $.regions %>
        workflow: project_update_per_region
        input:
          id: <% $.id %>
          name: <% $.name %>
          description: <% $.description %>
          status: <% $.status %>
          type: <% $.type %>
          owner: <% $.owner %>
          administrator: <% $.administrator %>
          region: <% $.region %>

  project_update_per_region:
    description: Updates the status of resources of a project on the identity service
    type: direct
    input:
      - id
      - name
      - description
      - status
      - type
      - owner
      - administrator
      - owner_role: 'c93214972afe4fccbdb78fea78c362cd'
      - administrator_role: 'de6ac1bc8882497f9c4b58cb668a0398'
      - region: 'cern'
    tasks:
      project_get:
        action: keystone.projects_get
        input:
          project: <% $.id %>
          action_region: <% $.region %>
        on-success:
          - update_project_name: <% not $.name.isEmpty() and $.name != task(project_get).result.name %>
          - update_project_description: <% not $.description.isEmpty() and $.description != task(project_get).result.description %>
          - update_project_status: <% not $.status.isEmpty() and $.status != task(project_get).result.status %>
          - update_project_type: <% not $.type.isEmpty() and $.type != task(project_get).result.type %>
          - get_project_owner: <% not $.owner.isEmpty() %>
          - get_project_administrator: <% not $.administrator.isEmpty() %>

      update_project_name:
        action: keystone.projects_update
        input:
          project: <% $.id %>
          name: <% $.name  %>
          action_region: <% $.region %>

      update_project_description:
        action: keystone.projects_update
        input:
          project: <% $.id %>
          description: <% $.description %>
          action_region: <% $.region %>

      update_project_status:
        action: keystone.projects_update
        input:
          project: <% $.id %>
          status: <% $.status %>
          action_region: <% $.region %>

      update_project_type:
        action: keystone.projects_update
        input:
          project: <% $.id %>
          type: <% $.type %>
          action_region: <% $.region %>

      get_project_owner:
        action: keystone.role_assignments_list
        input:
          role: <% $.owner_role %>
          project: <% $.id %>
          action_region: <% $.region %>
        publish:
          owners: <% task(get_project_owner).result.user.id %>
        on-success:
          - remove_previous_owner: <% not ($.owner in $.owners)  %>

      remove_previous_owner:
        with-items: old_owner in <% $.owners %>
        action: keystone.roles_revoke
        input:
          role: <% $.owner_role %>
          user: <% $.old_owner %>
          project: <% $.id %>
          action_region: <% $.region %>
        on-success:
          - set_project_owner

      set_project_owner:
        action: keystone.roles_grant
        input:
          role: <% $.owner_role %>
          user: <% $.owner %>
          project: <% $.id %>
          action_region: <% $.region %>

      get_project_administrator:
        action: keystone.role_assignments_list
        input:
          role: <% $.administrator_role %>
          project: <% $.id %>
          action_region: <% $.region %>
        publish:
          admins: <% task(get_project_administrator).result.group.id %>
        on-success:
          - remove_previous_admins: <% not ($.administrator in $.admins)  %>

      remove_previous_admins:
        with-items: old_admin in <% $.admins %>
        action: keystone.roles_revoke
        input:
          role: <% $.administrator_role %>
          group: <% $.old_admin %>
          project: <% $.id %>
          action_region: <% $.region %>
        on-success:
          - set_project_admins

      set_project_admins:
        action: keystone.roles_grant
        input:
          role: <% $.administrator_role %>
          group: <% $.administrator %>
          project: <% $.id %>
          action_region: <% $.region %>

  project_enable:
    description: Enables a project in all regions
    type: direct
    input:
      - project
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - identity_enable_per_region
      identity_enable_per_region:
        with-items: region in <% $.regions %>
        action: keystone.projects_update
        input:
          project: <% $.project %>
          enabled: True
          action_region: <% $.region %>

  project_disable:
    description: Disables a project in all regions
    type: direct
    input:
      - project
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - identity_disable_per_region
      identity_disable_per_region:
        with-items: region in <% $.regions %>
        action: keystone.projects_update
        input:
          project: <% $.project %>
          enabled: False
          action_region: <% $.region %>

  project_set_properties:
    description: Sets properties in all regions
    type: direct
    input:
      - project
      - properties
    tasks:
      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - set_project
      set_project:
        with-items: region in <% $.regions %>
        action: keystone.projects_update
        input: <% $.properties.mergeWith(dict(project => $.project, action_region => $.region)) %>

  project_list_properties:
    type: direct
    description: Retrieves the properties configured in the project
    input:
      - project
    output:
      properties: <% $.properties %>
    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_project
      get_project:
        action: keystone.projects_get
        input:
          action_region: <% $.default_region %>
          project: <% $.project %>
        publish:
          properties: <% task(get_project).result.delete('_info','_loaded','manager','links', 'domain_id','parent_id','is_domain','tags','id','name','description','enabled').items().select(dict(property_name => $.first(), property_value => $.last())) %>

  add_grants:
    type: direct
    description: Adds the role to the users and groups to the project specified on all regions
    input:
      - project
      - members: []
      - role: "Member"
    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_role_id

      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - get_role_id

      get_role_id:
        join: all
        action: keystone.roles_find
        input:
          action_region: <% $.default_region %>
          name: <% $.role %>
        publish:
          role_id: <% task(get_role_id).result.id %>
        on-success:
          - split_members

      split_members:
        with-items: member in <% $.members %>
        workflow: split_user_group
        input:
          member: <% $.member %>
        publish:
          users: <% task(split_members).result.user.where($) %>
          groups: <% task(split_members).result.group.where($) %>
        on-success:
          - add_user_grants
          - add_group_grants

      add_user_grants:
        with-items: region in <% $.regions %>
        workflow: add_user_grant_per_region
        input:
          role_id: <% $.role_id %>
          users: <% $.users %>
          project: <% $.project %>
          action_region: <% $.default_region %>

      add_group_grants:
        with-items: region in <% $.regions %>
        workflow: add_group_grant_per_region
        input:
          role_id: <% $.role_id %>
          groups: <% $.groups %>
          project: <% $.project %>
          action_region: <% $.default_region %>

  add_user_grant_per_region:
    type: direct
    input:
      - role_id
      - users: []
      - project
      - action_region
    tasks:
      add_grants:
        with-items: user in <% $.users %>
        action: keystone.roles_grant
        input:
          action_region: <% $.action_region %>
          role: <% $.role_id %>
          user: <% $.user %>
          project: <% $.project %>

  add_group_grant_per_region:
    type: direct
    input:
      - role_id
      - project
      - groups: []
      - action_region
    tasks:
      add_grants:
        with-items: group in <% $.groups %>
        action: keystone.roles_grant
        input:
          action_region: <% $.action_region %>
          role: <% $.role_id %>
          group: <% $.group %>
          project: <% $.project %>

  revoke_grants:
    type: direct
    description: Revoke the role to the users and groups to the project specified on all regions
    input:
      - project
      - members: []
      - role: "Member"
    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_role_id

      fetch_regions:
        workflow: fetch_regions
        publish:
          regions: <% task(fetch_regions).result.regions %>
        on-success:
          - get_role_id

      get_role_id:
        join: all
        action: keystone.roles_find
        input:
          action_region: <% $.default_region %>
          name: <% $.role %>
        publish:
          role_id: <% task(get_role_id).result.id %>
        on-success:
          - split_members

      split_members:
        with-items: member in <% $.members %>
        workflow: split_user_group
        input:
          member: <% $.member %>
        publish:
          users: <% task(split_members).result.user.where($) %>
          groups: <% task(split_members).result.group.where($) %>
        on-success:
          - revoke_user_grants
          - revoke_group_grants

      revoke_user_grants:
        with-items: region in <% $.regions %>
        workflow: revoke_user_grant_per_region
        input:
          role_id: <% $.role_id %>
          users: <% $.users %>
          project: <% $.project %>
          action_region: <% $.default_region %>

      revoke_group_grants:
        with-items: region in <% $.regions %>
        workflow: revoke_group_grant_per_region
        input:
          role_id: <% $.role_id %>
          groups: <% $.groups %>
          project: <% $.project %>
          action_region: <% $.default_region %>

  revoke_user_grant_per_region:
    type: direct
    input:
      - role_id
      - users: []
      - project
      - action_region
    tasks:
      revoke_grants:
        with-items: user in <% $.users %>
        action: keystone.roles_revoke
        input:
          action_region: <% $.action_region %>
          role: <% $.role_id %>
          user: <% $.user %>
          project: <% $.project %>

  revoke_group_grant_per_region:
    type: direct
    input:
      - role_id
      - project
      - groups: []
      - action_region
    tasks:
      revoke_grants:
        with-items: group in <% $.groups %>
        action: keystone.roles_revoke
        input:
          action_region: <% $.action_region %>
          role: <% $.role_id %>
          group: <% $.group %>
          project: <% $.project %>

  split_user_group:
    type: direct
    description: Helper workflow to split the member into a user or a group
    input:
      - member
    output:
      user: <% $.user %>
      group: <% $.group %>
    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_user_id
          - get_group_id

      get_user_id:
        action: keystone.users_find
        input:
          action_region: <% $.default_region %>
          name: <% $.member %>
        publish:
          user: <% task(get_user_id).result.id %>
        publish-on-error:
          user: null
        on-success:
          - final
        on-error:
          - final

      get_group_id:
        action: keystone.groups_find
        input:
          action_region: <% $.default_region %>
          name: <% $.member %>
        publish:
          group: <% task(get_group_id).result.id %>
        publish-on-error:
          group: null
        on-success:
          - final
        on-error:
          - final

      final:
        join: all
        action: std.noop

  check_grant:
    type: direct
    description: check the role to the users and groups to the project specified on default region
    input:
      - project
      - member
      - role: "Member"
    output:
      check: <% $.check %>

    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_role_id

      get_role_id:
        action: keystone.roles_find
        input:
          action_region: <% $.default_region %>
          name: <% $.role %>
        publish:
          role_id: <% task(get_role_id).result.id %>
        on-success:
          - role_check

      get_user_or_group:
        workflow: split_user_group
        input:
          member: <% $.member %>
        publish:
          user: <% task(get_user_or_group).result.user %>
          group: <% task(get_user_or_group).result.group %>
        on-success:
          - role_check

      role_check:
        join: all
        action: keystone.roles_check
        input:
          action_region: <% $.default_region %>
          project: <% $.project %>
          role: <% $.role_id %>
          user: <% $.user %>
          group: <% $.group %>
        publish:
          check: <% task(role_check).result %>
        publish-on-error:
          check: false
        on-success:
          - final
        on-error:
          - final

      final:
        action: std.noop

  list_grants:
    type: direct
    description: Retrieves the grants assigned to the project on default region
    input:
      - project
      - role: "Member"
    ouput:
      grants: <% $.grants %>

    tasks:
      get_default_region:
        workflow: get_default_region
        publish:
          default_region: <% task(get_default_region).result.region %>
        on-success:
          - get_role_id

      get_role_id:
        action: keystone.roles_find
        input:
          action_region: <% $.default_region %>
          name: <% $.role %>
        publish:
          role_name: <% $.role %>
          role_id: <% task(get_role_id).result.id %>
        on-success:
          - list_grants

      list_grants:
        action: keystone.role_assignments_list
        input:
          action_region: <% $.default_region %>
          role: <% $.role_id %>
          project: <% $.project %>
        publish:
          grants: <% task(list_grants).result.select(dict(name => switch($.containsKey("user") => $.user.id,$.containsKey("group") => $.group.id), project_id => $.scope.project.id, role_name => task(get_role_id).published.role_name)) %>
