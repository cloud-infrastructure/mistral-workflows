---
version: '2.0'

name: devel_service_delete
description: Workbook that contains the cleanup operations on services on a project

workflows:
  init:
    type: direct
    input:
      - id
    tasks:
      deletion_ordered_per_region:
        workflow: devel_service_utils.service_dependency
        input:
          workbook_name: devel_service_delete
          id: <% $.id %>

  nova:
    description: Deletes all resources of a project on the compute service per region
    type: direct
    input:
      - id
      - region
    tasks:
      # Get all VMs in the project
      get_vms:
        action: nova.servers_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          instances: <% task(get_vms).result.select(dict(id => $.id, name => $.name, status => $.status)) %>
        on-success:
          - delete_vms

      # Delete the VMs
      delete_vms:
        with-items: instance in <% $.instances %>
        workflow: service_delete.nova_delete_instance
        input:
          instance: <% $.instance %>
          region: <% $.region %>
        on-success:
          - quota_cleanup
        
      # Cleanup quota entries in nova
      quota_cleanup:
        action: nova.quotas_delete
        input:
          tenant_id: <% $.id %>
          action_region: <% $.region %>

  nova_delete_instance:
    type: direct
    description: Deletes an instance on the compute service
    input:
      - instance
      - region
      - skip_status: []
    tasks:
      check_instance_status:
        description: 'Skip status not supported by the APIs'
        action: std.noop
        on-success:
          - unlock_instance: <% not $.instance.status in $.skip_status %>
          - delete_instance: <% $.instance.status in $.skip_status %>

      unlock_instance:
        description: 'Unlocks the instance'
        action: nova.servers_unlock
        input:
          server: <% $.instance.id %>
          action_region: <% $.region %>
        on-success:
          - delete_instance
        on-error:
          - delete_instance

      delete_instance:
        action: nova.servers_delete
        input:
          server: <% $.instance.id %>
          action_region: <% $.region %>

  cinder:
    description: Deletes all resources of a project on the blockstorage service per region
    type: direct
    input:
      - id
      - region
    tasks:
      # Get all volumes snapshots in the project
      get_snapshots:
        action: cinder.volume_snapshots_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          snapshot_ids: <% task(get_snapshots).result.id %>
        on-success:
          - delete_snapshots
      # Delete the volume snapshots
      delete_snapshots:
        with-items: snapshot_id in <% $.snapshot_ids %>
        action: cinder.volume_snapshots_delete
        input:
          snapshot: <% $.snapshot_id %>
          action_region: <% $.region %>
        on-success:
          - get_volumes
      # Get all volumes in the project
      get_volumes:
        action: cinder.volumes_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          volume_ids: <% task(get_volumes).result.id %>
        on-success:
          - delete_volumes
      # Delete the volumes
      delete_volumes:
        with-items: volume_id in <% $.volume_ids %>
        action: cinder.volumes_delete
        input:
          volume: <% $.volume_id %>
          action_region: <% $.region %>
        on-success:
          - quota_cleanup
      # Cleanup quota entries in cinder
      quota_cleanup:
        join: all
        action: cinder.quotas_delete
        input:
          tenant_id: <% $.id %>
          action_region: <% $.region %>

  glance:
    description: Deletes all resources of a project on the image service per region
    type: direct
    input:
      - id
      - region
    tasks:
      # Get all images in the project
      get_images:
        action: glance.images_list
        input:
          filters:
            owner: <% $.id %>
          action_region: <% $.region %>
        publish:
          image_ids: <% task(get_images).result.where($.visibility='private').id %>
        on-success:
          - unprotect_images

      unprotect_images:
        with-items: image_id in <% $.image_ids %>
        action: glance.images_update
        input:
          image_id: <% $.image_id %>
          protected: false
          action_region: <% $.region %>
        on-success:
          - delete_images
        on-error:
          - delete_images

      delete_images:
        join: all
        with-items: image_id in <% $.image_ids %>
        action: glance.images_delete
        input:
          image_id: <% $.image_id %>
          action_region: <% $.region %>

  manila:
    description: Deletes all resources of a project on the fileshare service per region
    type: direct
    input:
      - id
      - region
    tasks:
      # Get all shares in the project
      get_snapshots:
        action: manila.share_snapshots_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          snapshot_ids: <% task(get_snapshots).result.id %>
        on-success:
          - delete_snapshots

      # Delete the share snapshots
      delete_snapshots:
        with-items: snapshot_id in <% $.snapshot_ids %>
        action: manila.share_snapshots_delete
        input:
          snapshot: <% $.snapshot_id %>
          action_region: <% $.region %>
        on-success:
          - get_shares

      # Get all shares in the project
      get_shares:
        action: manila.shares_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          share_ids: <% task(get_shares).result.id %>
        on-success:
          - delete_shares

      # Delete the shares
      delete_shares:
        with-items: share_id in <% $.share_ids %>
        action: manila.shares_delete
        input:
          share: <% $.share_id %>
          action_region: <% $.region %>
        on-success:
          - quota_cleanup

      # Cleanup quota entries in manila
      quota_cleanup:
        join: all
        action: manila.quotas_delete
        input:
          tenant_id: <% $.id %>
          action_region: <% $.region %>

  barbican:
    description: Deletes all resources of a project on the key_manager service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  neutron:
    description: Deletes all resources of a project on the network service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  s3:
    description: Deletes all resources of a project on the s3 service per region
    type: direct
    input:
      - id
      - region
    tasks:
      detect_region:
        action: std.noop
        on-success:
          - retrieve_access_key: <% not 'pdc' in $.region %>

      retrieve_access_key:
        workflow: secret_retrieve
        input:
          name: rgw_access_key
        publish:
          access_key: <% task(retrieve_access_key).result.payload %>
        on-success:
          - radosgw_delete_user

      retrieve_secret_key:
        workflow: secret_retrieve
        input:
          name: rgw_secret_key
        publish:
          secret_key: <% task(retrieve_secret_key).result.payload %>
        on-success:
          - radosgw_delete_user

      radosgw_delete_user:
        join: all
        action: radosgw.user_delete
        input:
          uid: <% $.id %>
          purge_data: true
          rgw_access: <% $.access_key %>
          rgw_secret: <% $.secret_key %>
          action_region: <% $.region %>
        on-success:
          - empty_task
        on-error:
          - empty_task

      empty_task:
        action: std.noop

  mistral:
    description: Deletes all resources of a project on the workflow service per region
    type: direct
    input:
      - id
      - region
    tasks:
      # TODO(jcastro) Add missing event trigger and cron trigger deletion
      # Get all workflows in the project
      get_workflows:
        action: mistral.workflows_list
        input:
          project_id: <% $.id %>
        publish:
          workflow_ids: <% task(get_workflows).result.id %>
        on-success:
          - delete_workflows
        on-error:
          # The service returns not found error if the filter does not match
          - empty_task
      # Delete all workflows
      delete_workflows:
        with-items: workflow_id in <% $.workflow_ids %>
        action: mistral.workflows_delete
        input:
          identifier: <% $.workflow_id %>
      empty_task:
        action: std.noop
