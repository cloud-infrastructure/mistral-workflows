---
version: '2.0'

name: cloud_dr_report
description: Workbook that reports status from services for disaster recovery

workflows:
  create_report_job:
    type: direct
    description: This workflow triggers a cronjob to send reports for DR.
    tasks:
      create_cron:
        description: 'Creates the cronjobs for the radosgw sync job'
        action: mistral.cron_triggers_create
        input:
          name: "cloud_dr_report_job"
          workflow_identifier: cloud_dr_report.report_job_global
          workflow_input: {}
          pattern: "0 8 * * *"

  report_job_global:
    type: direct
    description: This workflow reports the status of the essential infrastructure machines for disaster recovery
    input:
      - reports:
        - service: 'Cloud Control Plane'
          region: 'cern'
          expressions:
            - '^cci-keystone-'
            - '^cci-lb-'
            - '^cci-mq-'
            - '^cci-nova-'
            - '^cci-neutron-'
            - '^cci-ironic-'
        - service: 'Cloud Control Plane'
          region: 'pdc'
          expressions:
            - '.'
          tenant_id: 'bfc4ad73-6f80-4079-99fb-268a5aeff205'
      - mail: ['cloud-infrastructure-dr-report@cern.ch']
    tasks:
      launch_report_project:
        description: 'Fork to generate multiple reports'
        with-items: report in <% $.reports %>
        concurrency: 1
        workflow: cloud_dr_report.report_job_project
        input:
          report: <% $.report %>
          mail: <% $.mail %>

  report_job_project:
    type: direct
    description: Produces a report for the specific set of machines
    input:
      - report
      - params:
          search_opts:
            all_tenants: 1
      - mail: ['cloud-infrastructure-dr-report@cern.ch']
    tasks:
      init:
        action: std.noop
        publish:
          search_params: <% selectCase($.report.containsKey("tenant_id")).switchCase($.params.mergeWith({"search_opts" => {"tenant_id" => $.report.tenant_id}, "action_region" => $.report.region}),$.params.mergeWith({"action_region" => $.report.region})) %>
        on-success:
          - retrieve_machines

      retrieve_machines:
        description: 'Retrieve the list of instances for that report'
        action: nova.servers_list
        with-items: regex in <% $.report.expressions %>
        input: <% $.search_params.mergeWith({"search_opts" => {"name" => $.regex}}) %>
        publish:
          machines: <% task(retrieve_machines).result.flatten().select(dict(id => $.id, name => $.name, host => $.get("OS-EXT-SRV-ATTR:host"), avz => $.get("OS-EXT-AZ:availability_zone"), instance_name => $.get("OS-EXT-SRV-ATTR:instance_name"))) %>
        on-success:
          - send_mail_template

      send_mail_template:
        description: 'Retrieve template from Gitlab'
        workflow: send_mail_template
        input:
          to_addrs: <% $.mail %>
          template: cloud_dr_report_mail
          replacements:
            "{service_name}": <% $.report.service %>
            "{region_name}": <% $.report.region %>
            "{machines_txt}": <% $.machines.orderBy($.name).select(concat($.name+"\t"+$.avz+"\t"+$.host+"\t"+$.instance_name+"\n")).sum() %>
            "{machines_html}": <% $.machines.orderBy($.name).select(concat("<tr><td>"+$.name+"</td><td>"+$.avz+"</td><td>"+$.host+"</td><td>"+$.instance_name+"</td></tr>")).sum("<tr><th>Name</th><th>Availability Zone</th><th>Hypervisor</th><th>Instance ID</th></tr>") %>
