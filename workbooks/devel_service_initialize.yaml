---
version: '2.0'

name: devel_service_initialize
description: Workflow that initializes a project on the different services

workflows:
  base:
    description: Initializes the base services of a Project
    type: direct
    input:
      - id

    tasks:
      initialize_per_region:
        workflow: devel_service_utils.service_iterate
        input:
          workbook_name: devel_service_initialize
          id: <% $.id %>

  nova:
    description: Initializes a project on the compute service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      init:
        action: std.noop
        on-success:
          - set_default_quota_main: <% 'cern' in $.region %>
          - set_default_quota_other: <% not 'cern' in $.region %>

      set_default_quota_main:
        action: nova.quotas_update
        input:
          action_region: <% $.region %>
          tenant_id: <% $.id %>
          cores: 10
          ram: 20480
          instances: 5

      set_default_quota_other:
        action: nova.quotas_update
        input:
          action_region: <% $.region %>
          tenant_id: <% $.id %>
          cores: 0
          ram: 0
          instances: 0

  cinder:
    description: Initializes a project on the blockstorage service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      update_volume_global_quota:
        action: cinder.quotas_update
        input:
          action_region: <% $.region %>
          tenant_id: <% $.id %>
          volumes: 10
          gigabytes: 250
          snapshots: 20
          backups: 0
          backup_gigabytes: 0
        on-success:
          - set_default_quota_main: <% 'cern' in $.region %>

      set_default_quota_main:
        action: cinder.quotas_update
        input:
          action_region: <% $.region %>
          tenant_id: <% $.id %>
          volumes_standard: 10
          gigabytes_standard: 250
          snapshots_standard: -1
          volumes_io1: 1
          gigabytes_io1: 100
          snapshots_io1: -1
          volumes_crypt: 0
          gigabytes_crypt: 0
          snapshots_crypt: -1
          volumes_cp1: 0
          gigabytes_cp1: 0
          snapshots_cp1: -1
          volumes_cpio1: 0
          gigabytes_cpio1: 0
          snapshots_cpio1: -1
          volumes_cpio2: 0
          gigabytes_cpio2: 0
          snapshots_cpio2: -1
          volumes_cpio3: 0
          gigabytes_cpio3: 0
          snapshots_cpio3: -1
          volumes_io2: 0
          gigabytes_io2: 0
          snapshots_io2: -1
          volumes_io3: 0
          gigabytes_io3: 0
          snapshots_io3: -1
          volumes_hyperc: 0
          gigabytes_hyperc: 0
          snapshots_hyperc: -1

  glance:
    description: Initializes a project on the image service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  manila:
    description: Initializes a project on the fileshare service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      update_share_global_quota:
        action: manila.quotas_update
        input:
          tenant_id: <% $.id %>
          shares: 0
          snapshots: 0
          gigabytes: 0
          snapshot_gigabytes: 0
          action_region: <% $.region %>
        on-success:
          - retrieve_share_types

      retrieve_share_types:
        action: manila.share_types_list
        input:
          action_region: <% $.region %>
        publish:
          share_types: <% task(retrieve_share_types).result.id %>
        on-success:
          set_default_manila_quotas

      set_default_manila_quotas:
        with-items: share_type in <% $.share_types %>
        action: manila.quotas_update
        input:
          tenant_id: <% $.id %>
          share_type: <% $.share_type %>
          shares: 0
          snapshots: 0
          gigabytes: 0
          snapshot_gigabytes: 0
          action_region: <% $.region %>

  barbican:
    description: Initializes a project on the key_manager service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  neutron:
    description: Initializes a project on the network service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      set_default_neutron_quotas:
        action: neutron.update_quota
        input:
          project_id: <% $.id %>
          action_region: <% $.region %>
          body:
            quota:
              network: 0
              subnet: 0
              port: 10
              floatingip: 0
              router: 0
              security_group: 1
              security_group_rule: 4

  mistral:
    description: Initializes a project on the workflow service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  s3:
    description: Initializes a project on the S3 service and sets default quotas
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop
