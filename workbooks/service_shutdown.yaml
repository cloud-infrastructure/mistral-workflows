---
version: '2.0'

name: service_shutdown
description: Workbook that contains the disable operations on services on a project

workflows:
  init:
    type: direct
    input:
      - id
    tasks:
      shutdown_ordered_per_region:
        workflow: service_utils.service_dependency
        input:
          workbook_name: service_shutdown
          id: <% $.id %>

  nova:
    description: Disables the status of resources of a project on the compute service
    type: direct
    input:
      - id
      - region
    tasks:
      get_vms:
        action: nova.servers_list
        input:
          search_opts:
            all_tenants: true
            project_id: <% $.id %>
          action_region: <% $.region %>
        publish:
          instances: <% task(get_vms).result %>
        on-success:
          - stop_instances

      stop_instances:
        with-items: instance in <% $.instances %>
        workflow: service_shutdown.nova_stop_instance
        input:
          instance: <% $.instance %>
          region: <% $.region %>

  nova_stop_instance:
    type: direct
    description: Disables the status of resources on an instance on the compute service
    input:
      - instance
      - region
      - skip_status: ['ERROR', 'SUSPENDED', 'SHUTOFF']
    tasks:
      check_instance_status:
        description: 'Skip status not supported by the APIs'
        action: std.noop
        on-success:
          - stop_instance: <% not $.instance.status in $.skip_status %>

      stop_instance:
        description: 'Stops the machine'
        action: nova.servers_stop
        input:
          server: <% $.instance.id %>
          action_region: <% $.region %>

  cinder:
    description: Disables the status of resources of a project on the blockstorage service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  glance:
    description: Disables the status of resources of a project on the image service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  manila:
    description: Disables the status of resources of a project on the fileshare service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  barbican:
    description: Disables the status of resources of a project on the key_manager service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  neutron:
    description: Disables the status of resources of a project on the network service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop

  s3:
    description: Disables the status of resources of a project on the s3 service
    type: direct
    input:
      - id
      - region
    tasks:
      detect_region:
        action: std.noop
        on-success:
          - retrieve_access_key: <% not 'pdc' in $.region %>

      retrieve_access_key:
        workflow: secret_retrieve
        input:
          name: rgw_access_key
        publish:
          access_key: <% task(retrieve_access_key).result.payload %>
        on-success:
          - radosgw_suspend_user

      retrieve_secret_key:
        workflow: secret_retrieve
        input:
          name: rgw_secret_key
        publish:
          secret_key: <% task(retrieve_secret_key).result.payload %>
        on-success:
          - radosgw_suspend_user

      radosgw_suspend_user:
        join: all
        action: radosgw.user_set
        input:
          uid: <% $.id %>
          suspended: true
          rgw_access: <% $.access_key %>
          rgw_secret: <% $.secret_key %>
          action_region: <% $.region %>
        on-success:
          - empty_task
        on-error:
          - empty_task

      empty_task:
        action: std.noop

  mistral:
    description: Disables the status of resources of a project on the workflow service
    type: direct
    input:
      - id
      - region
    tasks:
      empty_task:
        action: std.noop
