---
version: '2.0'

name: radosgw_key_sync
description: Workbook that contains synchronization from keystone to radosgw keys

workflows:
  create_sync_job:
    type: direct
    description: This workflow triggers a cronjob to sync the keys between keystone and radosgw.
    tasks:
      create_cron:
        description: 'Creates the cronjobs for the radosgw sync job'
        action: mistral.cron_triggers_create
        input:
          name: "radosgw_key_sync_job"
          workflow_identifier: radosgw_key_sync.sync_job_main
          workflow_input: {}
          pattern: "*/15 * * * *"

  sync_job_main:
    type: direct
    description: This workflow syncs the keys on all the projects with s3quota tag
    input:
      - region: "cern"
    tasks:
      retrieve_all_credentials:
        description: 'Retrieves all ec2 credentials'
        action: keystone.credentials_list
        input:
          action_region: "<% $.region %>"
          type: "ec2"
        keep-result: false
        publish:
          raw_credentials: <% task(retrieve_all_credentials).result.select(dict(project_id => $.project_id, blob => json_parse($.blob))) %>
        on-success:
          - reduce_credentials_to_sync

      retrieve_all_projects:
        description: 'Retrieves all projects with cern_s3_quota as a tag'
        action: keystone.projects_list
        input:
          action_region: "<% $.region %>"
          domain: "default"
          tags_any: "cern_s3_quota"
          enabled: "true"
        keep-result: false
        publish:
          projects: <% task(retrieve_all_projects).result.select(dict(id => $.id, name => $.name)) %>
        on-success:
          - reduce_credentials_to_sync

      reduce_credentials_to_sync:
        join: all
        action: std.noop
        publish:
          credentials: <% let(projects => $.projects) -> $.raw_credentials.where($projects.id.contains($.project_id)).select(dict(access_key => $.blob.access, secret_key => $.blob.secret, user => $.project_id)) %>
        on-success:
          - launch_sync_project

      retrieve_access_key:
        description: 'Retrieves the access key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_access_key
        publish:
          rgw_access_key: <% task(retrieve_access_key).result.payload %>
        on-success:
          - launch_sync_project

      retrieve_secret_key:
        description: 'Retrieves the secret key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_secret_key
        publish:
          rgw_secret_key: <% task(retrieve_secret_key).result.payload %>
        on-success:
          - launch_sync_project

      launch_sync_project:
        join: all
        description: 'Fork to synchronize the projects'
        with-items: project in <% $.projects %>
        concurrency: 1
        workflow: radosgw_key_sync.sync_job_project
        input:
          project: <% $.project %>
          credentials: <% let(project_id => $.project.id) -> $.credentials.where($.user = $project_id) %>
          region: <% $.region %>
          rgw_access_key: <% $.rgw_access_key %>
          rgw_secret_key: <% $.rgw_secret_key %>

  sync_job_one:
    type: direct
    description: This workflow syncs the keys on one project specified by project_id
    input:
      - project_id
      - region: "cern"
    tasks:
      retrieve_project:
        description: 'Retrieves project information'
        action: keystone.projects_get
        input:
          action_region: "<% $.region %>"
          project: <% $.project_id %>
        publish:
          project: <% task(retrieve_project).result %>
        on-success:
          - retrieve_credentials_for_project

      retrieve_credentials_for_project:
        description: 'Retrieves ec2 credentials mapped to that project'
        action: keystone.credentials_list
        input:
          action_region: "<% $.region %>"
          type: "ec2"
        keep-result: false
        publish:
          raw_credentials: <% let(project_id => $.project_id) -> task(retrieve_credentials_for_project).result.where($.project_id = $project_id).select(dict(project_id => $.project_id, blob => json_parse($.blob))) %>
        on-success:
          - reduce_credentials_to_sync

      reduce_credentials_to_sync:
        action: std.noop
        publish:
          credentials: <% $.raw_credentials.select(dict(access_key => $.blob.access, secret_key => $.blob.secret, user => $.project_id)) %>
        on-success:
          - launch_sync_project

      retrieve_access_key:
        description: 'Retrieves the access key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_access_key
        publish:
          rgw_access_key: <% task(retrieve_access_key).result.payload %>
        on-success:
          - launch_sync_project

      retrieve_secret_key:
        description: 'Retrieves the secret key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_secret_key
        publish:
          rgw_secret_key: <% task(retrieve_secret_key).result.payload %>
        on-success:
          - launch_sync_project

      launch_sync_project:
        join: all
        description: 'Fork to synchronize the projects'
        workflow: radosgw_key_sync.sync_job_project
        input:
          project: <% $.project %>
          credentials: <% let(project_id => $.project.id) -> $.credentials.where($.user = $project_id) %>
          region: <% $.region %>
          rgw_access_key: <% $.rgw_access_key %>
          rgw_secret_key: <% $.rgw_secret_key %>

  force_sync_one:
    type: direct
    description: This workflow pushes all keys in keystone to radosgw on one project specified by project_id
    input:
      - project_id
      - region: "cern"
    tasks:
      retrieve_project:
        description: 'Retrieves project information'
        action: keystone.projects_get
        input:
          action_region: "<% $.region %>"
          project: <% $.project_id %>
        publish:
          project: <% task(retrieve_project).result %>
        on-success:
          - retrieve_credentials_for_project

      retrieve_credentials_for_project:
        description: 'Retrieves ec2 credentials mapped to that project'
        action: keystone.credentials_list
        input:
          action_region: "<% $.region %>"
          type: "ec2"
        keep-result: false
        publish:
          raw_credentials: <% let(project_id => $.project_id) -> task(retrieve_credentials_for_project).result.where($.project_id = $project_id).select(dict(project_id => $.project_id, blob => json_parse($.blob))) %>
        on-success:
          - reduce_credentials_to_sync

      reduce_credentials_to_sync:
        action: std.noop
        publish:
          credentials: <% $.raw_credentials.select(dict(access_key => $.blob.access, secret_key => $.blob.secret, user => $.project_id)) %>
        on-success:
          - force_sync_project

      retrieve_access_key:
        description: 'Retrieves the access key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_access_key
        publish:
          rgw_access_key: <% task(retrieve_access_key).result.payload %>
        on-success:
          - force_sync_project

      retrieve_secret_key:
        description: 'Retrieves the secret key from barbican'
        workflow: secret_retrieve
        input:
          name: rgw_secret_key
        publish:
          rgw_secret_key: <% task(retrieve_secret_key).result.payload %>
        on-success:
          - force_sync_project

      force_sync_project:
        join: all
        description: 'Fork to synchronize the projects'
        workflow: radosgw_key_sync.force_sync_project
        input:
          project: <% $.project %>
          credentials: <% let(project_id => $.project.id) -> $.credentials.where($.user = $project_id) %>
          region: <% $.region %>
          rgw_access_key: <% $.rgw_access_key %>
          rgw_secret_key: <% $.rgw_secret_key %>

  sync_job_project:
    type: direct
    description: Synchronizes a given projects with the credentials passed by parameter
    input:
      - project
      - credentials
      - rgw_access_key
      - rgw_secret_key
      - region: "cern"
    tasks:
      retrieve_keys_from_radosgw:
        description: 'Retrieves the keys stored in radosgw'
        action: radosgw.user_get
        input:
          uid: <% $.project.id %>
          stats: false
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>
        publish:
          radosgw_keys: <% task(retrieve_keys_from_radosgw).result.keys %>
        on-success:
          - calculate_differences

      calculate_differences:
        description: Calculates the keys that must be added and the ones that must be removed
        action: std.noop
        publish:
          add_keys:    <% let(lkeys => $.radosgw_keys, rkeys => $.credentials)  -> $rkeys.where($lkeys.len() <= 0 or not $lkeys.access_key.contains($.access_key)) %>
          remove_keys: <% let(lkeys => $.credentials,  rkeys => $.radosgw_keys) -> $rkeys.where($lkeys.len() <= 0 or not $lkeys.access_key.contains($.access_key)) %>
        on-success:
          - add_keys_to_radosgw: <% $.add_keys %>
          - remove_keys_from_radosgw: <% $.remove_keys %>

      add_keys_to_radosgw:
        description: Adds the key into radosgw
        with-items: key in <% $.add_keys %>
        concurrency: 1
        action: radosgw.user_key_create
        input: 
          uid:  <% $.project.id %>
          access_key: <% $.key.access_key %>
          secret_key: <% $.key.secret_key %>
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>

      remove_keys_from_radosgw:
        description: Removes the key from radosgw
        with-items: key in <% $.remove_keys %>
        concurrency: 1
        action: radosgw.user_key_remove
        input: 
          uid:  <% $.project.id %>
          access_key: <% $.key.access_key %>
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>

  force_sync_project:
    type: direct
    description: Synchronizes a given projects with the credentials passed by parameter
    input:
      - project
      - credentials
      - rgw_access_key
      - rgw_secret_key
      - region: "cern"
    tasks:
      retrieve_keys_from_radosgw_before:
        description: 'Retrieves the keys stored in radosgw'
        action: radosgw.user_get
        input:
          uid: <% $.project.id %>
          stats: false
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>
        publish:
          radosgw_keys_before: <% task(retrieve_keys_from_radosgw_before).result.keys %>
        on-success:
          - remove_keys_from_radosgw

      remove_keys_from_radosgw:
        description: Removes the key from radosgw
        with-items: key in <% $.radosgw_keys_before %>
        concurrency: 1
        action: radosgw.user_key_remove
        input: 
          uid:  <% $.project.id %>
          access_key: <% $.key.access_key %>
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>
        on-success:
          - add_keys_to_radosgw

      add_keys_to_radosgw:
        description: Adds the key into radosgw
        with-items: key in <% $.credentials %>
        concurrency: 1
        action: radosgw.user_key_create
        input: 
          uid:  <% $.project.id %>
          access_key: <% $.key.access_key %>
          secret_key: <% $.key.secret_key %>
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>

      retrieve_keys_from_radosgw_after:
        description: 'Retrieves the keys stored in radosgw'
        action: radosgw.user_get
        input:
          uid: <% $.project.id %>
          stats: false
          rgw_access: <% $.rgw_access_key %>
          rgw_secret: <% $.rgw_secret_key %>
          action_region: <% $.region %>
        publish:
          radosgw_keys_after: <% task(retrieve_keys_from_radosgw_after).result.keys %>
